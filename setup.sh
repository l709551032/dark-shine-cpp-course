#!/bin/bash

echo "setting ROOT ..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/setup.sh

export CPP_COURSE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export CPP_COURSE_BUILD_DIR="${CPP_COURSE_DIR}/../build"
export CPP_COURSE_INSTALL_DIR="${CPP_COURSE_DIR}/../install"
export CPP_COURSE_RUN_DIR="${CPP_COURSE_DIR}/../run"

echo "setting project in ${CPP_COURSE_DIR} ..."
mkdir -vp ${CPP_COURSE_BUILD_DIR}
mkdir -vp ${CPP_COURSE_INSTALL_DIR}
mkdir -vp ${CPP_COURSE_RUN_DIR}

export PATH="${CPP_COURSE_INSTALL_DIR}/:$PATH"
export LD_LIBRARY_PATH="${CPP_COURSE_INSTALL_DIR}:$LD_LIBRARY_PATH"
