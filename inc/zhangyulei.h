#ifndef ZHANG_YULEI_H
#define ZHANG_YULEI_H

#include "student.h"

class ZhangYulei : public Student
{
public:
    ZhangYulei();
    ZhangYulei(const ZhangYulei &old_zhangyulei);
    ~ZhangYulei();

    void SetCarNumber(TString new_car_number) {car_number_ = new_car_number;}
    TString GetCarNumber() const {return car_number_;}

    virtual TString GetStudentTitle() const {return "Genius student";}

    friend std::ostream& operator<<(std::ostream &os, const ZhangYulei &zhang_yulei);

private:
    TString car_number_;
};

#endif
