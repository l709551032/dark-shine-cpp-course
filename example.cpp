#include <iostream>
#include <vector>

#include "student.h"
#include "zhangyulei.h"

int main(int argc, char *argv[])
{

    Student chen_xiang("Chen Xiang", Student::dMale);
    std::cout << "New student: " << chen_xiang.GetStudentName() << std::endl;
    chen_xiang.SetStudentOffice(607);
    std::cout << "Office of student " << chen_xiang.GetStudentName() << ": " << *chen_xiang.GetStudentOffice() << std::endl;

    Student mo_cen(chen_xiang);
    mo_cen.SetStudentName("Mo Cen");
    std::cout << "New student: " << mo_cen.GetStudentName() << std::endl;
    std::cout << "Office of student " << mo_cen.GetStudentName() << ": " << *mo_cen.GetStudentOffice() << std::endl;

    mo_cen.AddStudentFriend(chen_xiang);
    for(const auto friend_of_student_ : mo_cen.GetFriendsOfStudent())
        std::cout << "Friend of " << mo_cen.GetStudentName() << ": " << friend_of_student_->GetStudentName() << std::endl;

    chen_xiang.SetStudentName("Xiang Chen");
    for(const auto friend_of_student_ : mo_cen.GetFriendsOfStudent())
        std::cout << "Friend of " << mo_cen.GetStudentName() << ": " << friend_of_student_->GetStudentName() << std::endl;

    std::vector<Student> students;
    students.push_back(mo_cen);
    students.push_back(chen_xiang);
    for(const auto &student : students) std::cout << student;

    { 
        Student liu_danning("Liu Danning", Student::dFemale);
        liu_danning.AddStudentFriend(chen_xiang);
        liu_danning.AddStudentFriend(mo_cen);
        liu_danning.AddStudentFriend(liu_danning);
        std::cout << liu_danning << mo_cen;
    }

    std::cout << mo_cen;

    ZhangYulei zhang_yulei;
    std::cout << zhang_yulei;

    ZhangYulei zhang_yulei_at_JS(zhang_yulei);
    zhang_yulei_at_JS.SetCarNumber("苏XXXXXX");
    std::cout << zhang_yulei_at_JS;
}
