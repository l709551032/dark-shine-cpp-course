# Dark Shine C++ Course
C++ course for Dark Shine project

[[_TOC_]]

## How to setup & run
### How to setup
In some path you want to run the project:
```shell script
mkdir <path>
cd <path>
git clone -b playground git@gitlab.com:Biblehome/dark-shine-cpp-course.git source
cd source
source setup.sh
```
Or if you don't have a GitLab account:
```shell script
git clone -b playground https://gitlab.com/Biblehome/dark-shine-cpp-course.git source
```
### How to run
Then:
```shell script
cd ../build
cmake -DCMAKE_INSTALL_PREFIX=../install ../source
make -j100
make install
cd ../run
example
```
Each time you edit the codes, in build/:
```shell script
make install
```

## How to develop
Make sure you have a GitLab account, then fork the project:

![fork project step 1](https://gitlab.com/Biblehome/dark-shine-cpp-course/raw/master/fig/fork1.jpg)
![fork project step 2](https://gitlab.com/Biblehome/dark-shine-cpp-course/raw/master/fig/fork2.jpg)
